@extends('layout/main')

@section('title', 'Daftar Barang')

@section('container')

    <div class="container">
        <div class="row">
            <div class="col-10">  
                <h1 class="mt-3">List Barang-barang</h1>
                <a href="/barang/create" class="btn btn-success plus float-right my-3">Tambah Barang</a>
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Kode Barang</th>
                            <th scope="col">Nama Barang</th>
                            <th scope="col">Deskripsi</th>
                            <th scope="col">Stok Barang</th>
                            <th scope="col">Harga Barang</th> 
                            <th scope="col">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach( $barang as $brg)
                        <tr>
                            <th scope="row">{{ $loop->iteration }}</th>
                            <td>{{ $brg->kode_barang }}</td>
                            <td>{{ $brg->nama_barang }}</td>
                            <td>{{ $brg->deskripsi }}</td>
                            <td>{{ $brg->stok_barang }}</td>
                            <td>{{ $brg->harga_barang }}</td>
                            <td>
                                <a href="{{ $brg->id }}/edit" class="btn btn-warning">Edit</a>
                                <form method="POST" action="{{ $brg-> id }}" class="d-inline">
                                    <input type="hidden" name="_method" value="delete" />
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>

                </table>
            </div>
        </div>
    </div>
@endsection
