@extends('layout/main')


@section('container')
<title>Form Edit Data Barang </title>
     <div class="container">
        <div class="row">
            <div class="col-12">  
                <h1 class="mt-3">Form Edit Barang</h1>
                <form method="post" action="/barang/{{ $barang->id }}"  >
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="_method" value="patch" />
                    <div class="form-group">
                        <label for="kode_barang" class="form-label">Kode Barang</label>
                        <input type="text" class="form-control" id="kode_barang" placeholder="Masukan Kode Barang" name="kode_barang" value="{{$barang->kode_barang}}">
                    </div>
                    <div class="form-group">
                        <label for="nama_barang" class="form-label">Nama Barang</label>
                        <input type="text" class="form-control" id="nama_barang" placeholder="Masukan Nama Barang" name="nama_barang" value="{{$barang->nama_barang}}">
                    </div>
                    <div class="form-group">
                        <label for="deskripsi" class="form-label">Deskripsi</label>
                        <input type="text" class="form-control" id="deskripsi" placeholder="Masukan Deskripsi" name="deskripsi" value="{{$barang->deskripsi}}">
                    </div>
                    <div class="form-group">
                        <label for="stok_barang" class="form-label">Stok Barang</label>
                        <input type="text" class="form-control" id="stok_barang" placeholder="Masukan Stok Barang" name="stok_barang" value="{{$barang->stok_barang}}">
                    </div>
                    <div class="form-group">
                        <label for="harga_barang" class="form-label">Harga Barang</label>
                        <input type="text" class="form-control" id="harga_barang" placeholder="Masukan Harga Barang" name="harga_barang" value="{{$barang->harga_barang}}">
                    </div>
                    <div>
                        <button type="submit" class="btn btn-primary">Update</button>
                        <a href="/barang" class="btn btn-success my-3">Back</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection